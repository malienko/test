<?php

use Illuminate\Http\Request;


Route::get('service', 'LinkServiceController@index');
Route::post('service/registrations', 'ClientController@store');
Route::put('service/deposits', 'WalletController@update');
Route::get('service/currencies', 'CurrencyController@index');
Route::get('service/rates', 'RateController@index');
Route::get('service/rates/{rate}', 'RateController@show');
Route::post('service/rates', 'RateController@store');
Route::post('service/transfers', 'TransactionController@store');

Route::post('reports', 'ReportController@generateReport');