<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;
use App\Currency;

class RateController extends Controller
{
    public function index()
    {
        return Rate::all();
    }

    public function show(Rate $rate)
    {
        return $rate;
    }

    public function store(Request $request)
    {
        $currency = Currency::getByCode($request->currency);

        if (is_null($currency)) {
            return response()->json('Incorrect currency code', 404);
        }

        $rate = Rate::createUniqByDay($request, $currency);

        if (!$rate) {
            return response()->json('Rate is already created', 404);
        }

        return response()->json($rate, 201);
    }
}
