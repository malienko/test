<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wallet_id')->index()->comment('Wallet');
            $table->decimal('amount', 20, 4)->comment('Transfer amount');
            $table->unsignedInteger('currency_id')->index()->comment('Transfer currency');
            $table->enum('source', ['deposit', 'client'])->comment('Source of operation');
            $table->unsignedInteger('other_wallet_id')->index()->nullable()->comment('Other wallet');
            $table->date('date')->сomment('Date');
            $table->timestamp('created_at');

            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('other_wallet_id')->references('id')->on('wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
