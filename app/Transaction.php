<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Currency;
use App\Wallet;
use App\Rate;

class Transaction extends Model
{
    protected $fillable = ['wallet_id', 'amount', 'currency_id', 'operation', 'source'];

    public static function deposit(Request $request, Wallet $wallet)
    {
        $data = [
            'account' => $wallet->account + $request->amount,
            'wallet_id' => $wallet->id,
            'transfer_amount' => $request->amount,
            'transfer_currency_id' => $wallet->currency_id
        ];

        DB::transaction(function () use ($data) {
            DB::table('wallets')->where('id', $data['wallet_id'])->update(['account' => $data['account']]);
            DB::table('transactions')->insert([
                'wallet_id' => $data['wallet_id'],
                'amount' => $data['transfer_amount'],
                'currency_id' => $data['transfer_currency_id'],
                'source' => 'deposit',
                'date' => date('Y-m-d'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        });

        return $request->amount;
    }

    public static function transfer(Request $request, Wallet $senderWallet, Wallet $recipientWallet, Currency $transferCurrency, $senderAmount, $recipientAmount)
    {
        $data = [
            'sender_wallet_id' => $senderWallet->id,
            'sender_wallet_account' => $senderWallet->account - $senderAmount,
            'recipient_wallet_id' => $recipientWallet->id,
            'recipient_wallet_account' => $recipientWallet->account + $recipientAmount,
            'transfer_amount' => $request->amount,
            'transfer_currency_id' => $transferCurrency->id
        ];

        $transaction = DB::transaction(function () use ($data) {
            DB::table('wallets')->where('id', $data['sender_wallet_id'])->update(['account' => $data['sender_wallet_account']]);
            DB::table('wallets')->where('id', $data['recipient_wallet_id'])->update(['account' => $data['recipient_wallet_account']]);
            DB::table('transactions')->insert([
                'wallet_id' => $data['sender_wallet_id'],
                'amount' => $data['transfer_amount'] * -1,
                'currency_id' => $data['transfer_currency_id'],
                'source' => 'client',
                'other_wallet_id' => $data['recipient_wallet_id'],
                'date' => date('Y-m-d'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
            DB::table('transactions')->insert([
                'wallet_id' => $data['recipient_wallet_id'],
                'amount' => $data['transfer_amount'],
                'currency_id' => $data['transfer_currency_id'],
                'source' => 'client',
                'other_wallet_id' => $data['sender_wallet_id'],
                'date' => date('Y-m-d'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        });

        return $request->amount;
    }

    public static function getTransactionByWallet(Wallet $wallet, $date_from, $date_to)
    {
        return DB::table('transactions')
            ->join('currencies', 'transactions.currency_id', '=', 'currencies.id')
            ->select('transactions.*', 'currencies.code')
            ->where('wallet_id', $wallet->id)
            ->when(!is_null($date_from), function ($query) use ($date_from) {
                return $query->where('date', '>=', $date_from);
            })
            ->when(!is_null($date_to), function ($query) use ($date_to) {
                return $query->where('date', '<=', $date_to);
            })
            ->get();
    }

    public static function getTransactionTotal(Wallet $wallet, $date_from, $date_to)
    {
        return DB::table('transactions')
            ->join('currencies', 'transactions.currency_id', '=', 'currencies.id')
            ->join('wallets', 'wallets.id', '=', 'transactions.wallet_id')
            ->join('rates as r', function($join)
            {
                $join->on('r.currency_id', '=', 'transactions.currency_id')
                    ->on('r.date', '=', 'transactions.date');
            })
            ->join('rates as r1', function($join)
            {
                $join->on('r1.currency_id', '=', 'wallets.currency_id')
                    ->on('r1.date', '=', 'transactions.date');
            })
            ->select('transactions.*', 'currencies.code')
            ->select(DB::raw('transactions.amount * r.rate as amount_rate_usd, transactions.amount * r.rate / r1.rate as amount_rate_wallet'))
            ->where('transactions.wallet_id', $wallet->id)
            ->when(!is_null($date_from), function ($query) use ($date_from) {
                return $query->where('transactions.date', '>=', $date_from);
            })
            ->when(!is_null($date_to), function ($query) use ($date_to) {
                return $query->where('transactions.date', '<=', $date_to);
            })
            ->get();
    }
}
