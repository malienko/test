<?php

namespace App\Http\Controllers;

use App\Client;
use App\Currency;
use App\Rate;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function update(Request $request)
    {
        $client = Client::getByLogin($request->login);

        if (is_null($client)) {
            return response()->json('Login is not found', 404);
        }

        $wallet = Wallet::find($client->wallet_id);

        if (is_null($wallet)) {
            return response()->json('Wallet is not found', 404);
        }

        $transaction = Wallet::deposit($request, $wallet);

        if (is_null($transaction)) {
            return response()->json('Transaction failed. Please try again later', 404);
        }

        return response()->json(['payment' => number_format($transaction, 4)], 201);
    }
}
