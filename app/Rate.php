<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Currency;

class Rate extends Model
{
    protected $fillable = ['currency_id', 'rate', 'date'];

    public static function createUniqByDay(Request $request, Currency $currency)
    {
        $rate = Rate::getRateByDate($currency, $request->date);

        if (is_null($rate)) {
            return Rate::create([
                'currency_id' => $currency->id,
                'rate' => $request->rate,
                'date' => $request->date
            ]);
        } else {
            return false;
        }
    }

    public static function getRateByDate(Currency $currency, $date)
    {
        return Rate::where('currency_id', $currency->id)->whereDate('date', $date)->first();
    }
}
