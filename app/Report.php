<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public static function getReport(Wallet $wallet, $date_from, $date_to)
    {
        return Transaction::getTransactionByWallet($wallet, $date_from, $date_to);
    }

    public static function getReportTotalByUSD(Wallet $wallet, $date_from, $date_to)
    {
        $amount_rates = Transaction::getTransactionTotal($wallet, $date_from, $date_to);

        return [
            'amount_rate_usd' => $amount_rates->sum('amount_rate_usd'),
            'amount_rate_wallet' => $amount_rates->sum('amount_rate_wallet'),
        ];
    }
}
