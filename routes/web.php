<?php

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/', function () {
    return view('api');
});

Route::get('/reports/', function() {
    return view('index_reports');
});

Route::get('/downloads/{file}', 'DownloadController@download');

Route::post('/reports/', 'ReportController@index');
