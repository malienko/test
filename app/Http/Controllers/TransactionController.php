<?php

namespace App\Http\Controllers;

use App\Client;
use App\Currency;
use App\Rate;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function store(Request $request)
    {
        // Отправитель
        $sender = Client::getByLogin($request->login);

        if (!is_object($sender)) {
            return response()->json('Sender login is not found', 404);
        }

        $senderWallet = Wallet::find($sender->wallet_id);

        if (!is_object($senderWallet)) {
            return response()->json('Sender wallet is not found', 404);
        }

        $senderCurrency = Currency::find($senderWallet->currency_id);

        $senderRate = Rate::getRateByDate($senderCurrency, date('Y-m-d'));

        if (!is_object($senderRate)) {
            return response()->json('Incorrect currency code', 404);
        }

        // Получатель
        $recipient = Client::getByLogin($request->recipient);

        if (!is_object($recipient)) {
            return response()->json('Recipient login is not found', 404);
        }

        $recipientWallet = Wallet::find($recipient->wallet_id);

        if (!is_object($recipientWallet)) {
            return response()->json('Recipient wallet is not found', 404);
        }

        $recipientCurrency = Currency::find($recipientWallet->currency_id);

        $recipientRate = Rate::getRateByDate($recipientCurrency, date('Y-m-d'));

        if (!is_object($recipientRate)) {
            return response()->json('Incorrect currency code', 404);
        }

        // Детали перевода
        if (!in_array($request->currency, [$senderCurrency->code, $recipientCurrency->code])) {
            return response()->json('Incorrect currency code', 404);
        }

        $transferCurrency = $request->currency == $senderCurrency->code ? $senderCurrency : $recipientCurrency;

        // Расчёты перевода
        if ($transferCurrency->id == $senderCurrency->id) {
            $senderAmount = $request->amount;
            $recipientAmount = $request->amount * $senderRate->rate / $recipientRate->rate;
        } else {
            $senderAmount = $request->amount * $recipientRate->rate / $senderRate->rate;
            $recipientAmount = $request->amount;
        }

        if ($senderWallet->account < $senderAmount) {
            return response()->json('Insufficient funds', 404);
        }

        $transfer = Transaction::transfer(
            $request,
            $senderWallet,
            $recipientWallet,
            $transferCurrency,
            $senderAmount,
            $recipientAmount
        );

        return response()->json($transfer, 201);
    }
}
