<?php

namespace App\Http\Controllers;

use App\Client;
use App\Currency;
use App\Report;
use App\Wallet;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $result = $request->session()->all();
        $token = $result['_token'];

        $client = Client::getByLogin($request->login);

        if (!is_object($client)) {
            $content = [ 'error' => true, 'message' => 'Login is not found' ];
        }

        $wallet = Wallet::find($client->wallet_id);

        $currency = Currency::find($wallet->currency_id);

        $report = Report::getReport($wallet, $request->date_from, $request->date_to);

        $link = ReportController::generateCSV($request, $report);

        $amount_rates = Report::getReportTotalByUSD($wallet, $request->date_from, $request->date_to);

        return view('reports',[
            'token'=>$token,
            'data_row' => $report,
            'currency' => $currency->code,
            'total_usd' => $amount_rates['amount_rate_usd'],
            'total_wallet' => $amount_rates['amount_rate_wallet'],
            'link' => $link
        ]);
    }

    private function generateCSV(Request $request, $report)
    {
        $file_path = storage_path('app/reports/');

        if (!is_dir($file_path)) {
            mkdir($file_path, 0777, 1);
        }

        $file_name = collect($request->login, $request->date_from, $request->date_to)->implode('.');
        $file_path .= $file_name.'.csv';

        $fhandle = fopen($file_path, 'w');

        $title = [
            'Date operation',
            'Operation',
            'Amount',
            'Currency'
        ];

        fputcsv($fhandle, $title);

        $collection = collect($report);

        $collection->reduce(function ($result, $item) use($fhandle) {
            $line = [
                $item->created_at,
                $item->source,
                $item->amount,
                $item->code
            ];

            fputcsv($fhandle, $line);

            return $result;
        });

        fclose($fhandle);

        return $file_name.'.csv';
    }
}
