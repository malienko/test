<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Reports</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>.title{margin: 20px 0px;color: #636b6f;font-family: 'Raleway', sans-serif;font-weight: 100;font-size: 84px;text-align: center;}.filters{margin: 50px 0px;}.filters .form-group { margin-right: 20px; float: left; }.filters .form-group-right{float:right;}.filters .login, .filters .date-from, .filters .date-to{min-width:250px;}.clear{clear:both;}table{width:100%;}label{display: inline-flex;margin-bottom: .5rem;margin-top: .5rem;}.download .download-layout{text-align: right;}</style>

    <script type="application/javascript">
        $(document).ready(function() {
            $('#report').DataTable(

                {

                    "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
                    "iDisplayLength": 5
                }
            );
        } );


        function checkAll(bx) {
            var cbs = document.getElementsByTagName('input');
            for(var i=0; i < cbs.length; i++) {
                if(cbs[i].type == 'checkbox') {
                    cbs[i].checked = bx.checked;
                }
            }
        }
    </script>
</head>
<body>
<div>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <div class="container"><div class="title">Reports</div></div>

    <div class="container">
        <div class="filters">
            <form method="post" name="report" role="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="login">
                        <label for="login">User login</label>
                        <input name="login" type="text" class="form-control" id="login" aria-describedby="loginHelp" placeholder="Enter login">
                    </div>
                </div>
                <div class="form-group">
                    <div class="date-from">
                        <label for="date_from">Date from</label>
                        <div class='input-group date' id='datetimepicker1'>
                            <input id="date_from" name="date_from" type='text' class="form-control" placeholder="yyyy-mm-dd" />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="date-to">
                        <label for="date_to">Date to</label>
                        <div class='input-group date' id='datetimepicker2'>
                            <input id="date_to" name="date_to" type='text' class="form-control" placeholder="yyyy-mm-dd" />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="btn-form">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <table id="report" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Date operation</th>
                    <th>Operation</th>
                    <th>Amount</th>
                    <th>Currency</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
