<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Client;
use App\Wallet;

class ClientController extends Controller
{
    public function store(Request $request)
    {
        $checkLogin = Client::checkLogin($request->login);

        if ($checkLogin) {
            return response()->json('Login is already exists', 403);
        }

        $currency = Currency::getByCode($request->currency);

        if (is_null($currency)) {
            return response()->json('Incorrect currency code', 404);
        }

        $wallet = Wallet::create([
            "account" => 0.0,
            "currency_id" => $currency->id
        ]);

        $data = array_merge($request->toArray(), [
            'currency_id' => $currency->id,
            'wallet_id' => $wallet->id
        ]);

        $client = Client::create($data);

        return response()->json($client, 201);
    }
}
