<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LinkServiceController extends Controller
{
    public function index()
    {
        $service = [
            'get:currencies' => [
                'url' => url('/').'api/service/currency/',
                'method' => 'get',
                'params' => null
            ],
            'get:rates' => [
                'url' => url('/').'api/service/rates/',
                'method' => 'get',
                'params' => null
            ],
            'get:rates/{id}' => [
                'url' => url('/').'api/service/rates/{id}',
                'method' => 'get',
                'params' => ['id']
            ],
            'post:rates' => [
                'url' => url('/').'api/service/rates/',
                'method' => 'post',
                'params' => [
                    'currency' => 'Dictionary. Code '.url('/').'api/service/currency/',
                    'rate' => 'decimal(10, 4)',
                    'date' => 'format(yyyy-mm-dd)'
                ]
            ],
            'post:registrations' => [
                'url' => url('/').'api/service/registrations/',
                'method' => 'post',
                'params' => [
                    'login' => 'string. Uniq value',
                    'first_name' => 'string',
                    'country' => 'string',
                    'city' => 'string',
                    'currency' => 'Dictionary. Code '.url('/').'api/service/currency/'
                ]
            ],
            'post:deposits' => [
                'url' => url('/').'api/service/deposits/',
                'method' => 'post',
                'params' => [
                    'login' => 'string. Uniq value',
                    'amount' => 'decimal(10, 4)'
                ]
            ],
            'post:transfers' => [
                'url' => url('/').'api/service/transfers/',
                'method' => 'post',
                'params' => [
                    'login' => 'string. Uniq value',
                    'recipient' => 'string. Uniq value',
                    'amount' => 'decimal(10, 4)',
                    'currency' => 'Dictionary. Code '.url('/').'api/service/currency/'
                ]
            ]
        ];

        return response()->json($service, 200);
    }
}
