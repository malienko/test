<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['name', 'code'];

    public static function getByCode($code)
    {
        return Currency::where('code', $code)->first();
    }
}
