<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    function download($file_name)
    {
        $file_path = storage_path('app/reports/'.$file_name);
        return response()->download($file_path);
    }
}
