<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Wallet;

class Wallet extends Model
{
    protected $fillable = ['account', 'currency_id'];

    public static function deposit(Request $request, Wallet $wallet)
    {
        return Transaction::deposit($request, $wallet);
    }
}
