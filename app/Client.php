<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['login', 'first_name', 'country', 'city', 'wallet_id'];

    public static function checkLogin($login)
    {
        return Client::where('login', $login)->exists();
    }

    public static function getByLogin($login)
    {
        return Client::where('login', $login)->first();
    }
}
